FROM ubuntu:18.04

RUN apt-get update \
    && apt-get upgrade -y

RUN apt-get -y install build-essential \
    libcurl4-openssl-dev \
    libboost-regex-dev \
    libjsoncpp-dev \
    librhash-dev \
    libtinyxml2-dev \
    libhtmlcxx-dev \
    libboost-system-dev \
    libboost-filesystem-dev \
    libboost-program-options-dev \
    libboost-date-time-dev \
    libboost-iostreams-dev help2man \
    cmake \
    libssl-dev \
    pkg-config \
    zlib1g-dev \
    wget \
    unzip

# For some unfathomable reason the makers of lgogdownloader decided to make logging in interactive and not accept params.
# Running it with expect is a big fat ugly hack to circumvent this.
RUN DEBIAN_FRONTEND=noninteractive apt-get -yq install expect

# Install GOG downloader.
RUN mkdir /data
WORKDIR /data
RUN wget https://sites.google.com/site/gogdownloader/lgogdownloader-3.7.tar.gz;
RUN tar -xvzf lgogdownloader-3.7.tar.gz
WORKDIR /data/lgogdownloader-3.7

# Copy lgogdownloader patch to container
COPY ./patch /patch
RUN patch -p0 < /patch/login-envvars.patch

RUN mkdir build
WORKDIR /data/lgogdownloader-3.7/build
RUN cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release -DUSE_QT_GUI=OFF
RUN make
RUN make install

# For some weird reason, the system does not recognise starbound-server. Made a script for it. Copy it.
RUN mkdir -p /data/starbound/data/noarch/game/linux
# todo: move to after deploy ?
COPY ./install-starbound.sh /data/starbound
COPY ./start-starbound.sh /data/starbound/data/noarch/game/linux
#COPY ./starbound_1_4_4_34261.sh /data/starbound

RUN apt-get remove -y cmake \
    wget \
    build-essential \
    libcurl4-openssl-dev \
    libboost-regex-dev \
    libjsoncpp-dev \
    liboauth-dev \
    librhash-dev \
    libtinyxml2-dev \
    libhtmlcxx-dev \
    libboost-system-dev \
    libboost-filesystem-dev \
    libboost-program-options-dev \
    libboost-date-time-dev \
    libboost-iostreams-dev \
    help2man cmake libssl-dev \
    pkg-config git man \
    && apt autoremove --purge -y \
    && apt clean \
    && rm -rf /var/lib/apt/lists

